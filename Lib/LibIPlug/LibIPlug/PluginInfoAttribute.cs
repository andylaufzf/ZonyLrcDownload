﻿/*
 * 描述：定义了插件描述信息
 * 作者：Zony
 * 创建日期：2016/09/29
 * 最后修改日期：2016/09/29
 * 版本：v1.0
 */
using System;

namespace LibIPlug
{
    #region 插件描述信息
    /// <summary>
    /// 插件描述信息
    /// </summary>
    public class PluginInfoAttribute : Attribute
    {
        public PluginInfoAttribute() { }
        public PluginInfoAttribute(string name, string version, string author, string descript, int ptype)
        {
            _Name = name;
            _Version = version;
            _Author = author;
            _Descript = descript;
            _Ptype = ptype;
        }

        public string Name
        {
            get
            {
                return _Name;
            }
        }

        public string Version
        {
            get
            {
                return _Version;
            }
        }

        public string Author
        {
            get
            {
                return _Author;
            }
        }

        public string Descript
        {
            get
            {
                return _Descript;
            }
        }

        public int Ptype
        {
            get
            {
                return _Ptype;
            }
        }

        private string _Name = "";
        private string _Version = "";
        private string _Author = "";
        private string _Descript = "";
        private int _Ptype;
    }
    #endregion
}