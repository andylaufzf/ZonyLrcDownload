﻿/*
 * 描述：所有插件接口定义。
 * 作者：Zony
 * 创建日期：2016/09/29
 * 最后修改日期：2016/09/29
 * 版本：v1.0
 */

namespace LibIPlug
{
    #region 插件接口
    public interface IPlugin
    {
        PluginInfoAttribute PluginInfo { get; set; }
        /// <summary>
        /// 歌词下载接口
        /// </summary>
        /// <param name="filePath">歌曲文件路径</param>
        /// <param name="lrcData">的传出歌词数据</param>
        /// <param name="iThread">并发链接设置</param>
        /// <returns></returns>
        bool Down(string filePath, ref byte[] lrcData,int iThread);
    }
    #endregion

    #region 扩展插件
    public interface IPlugin_Hight
    {
        PluginInfoAttribute PluginInfo { get; set; }
        void Init(ref ResourceModule module);
    }
    #endregion

    #region 音乐信息读取插件
    public interface IPlugin_MusicInfo
    {
        PluginInfoAttribute PluginInfo { get; set; }
        void ReadInfo(string filePath,out string musicTitle,out string artist);
    }
    #endregion
}
